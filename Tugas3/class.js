class Animal {
    constructor(name) {
        this.onta = name;
        this.melon = 4;
        this.arab = false;
    } get name() {
        return this.onta;
    } get legs() {
        return this.melon;
    }
}
class Ape extends Animal {
    constructor(name) {
        super(name)
        this.onta = name;
        this.melon = 2;
    }
    yell() { return console.log("Auooo"); }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        this.onta = name;
    }
    jump() { return console.log("hop hop"); }
}

let sheep = new Animal("shaun");
console.log(sheep.onta) // "shaun"
console.log(sheep.melon) // 4
console.log(sheep.arab) // false
console.log("");

let sungokong = new Ape("kera sakti")
console.log(sungokong.onta); //"kera sakti"
console.log(sungokong.melon); //2
console.log(sungokong.arab) // false
sungokong.yell() // "Auooo"
console.log("");

let kodok = new Frog("buduk")
console.log(kodok.onta); //buduk
console.log(kodok.melon); //4
console.log(kodok.arab) // false
kodok.jump() // "hop hop"
console.log("");

//02.
class Clock {
    constructor({ template }) {
        this.template = template;
    }

    render() {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        let output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}
var clock = new Clock({ template: 'h:m:s' });
clock.start();