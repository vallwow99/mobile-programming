import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';

export default function Awal() {
    return (
        <View style={styles.canvas} >
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.title}>DATA USER</Text>
                </View>
                <TouchableOpacity style={styles.btnSignin}
                    onPress={() => document.location.href = 'Login.js'}>
                    <Text style={styles.signin}>SIGN IN</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    canvas: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
    },
    container: {
        width: 360,
        height: 640,
        backgroundColor: '#1ED0A6',
        alignItems: 'center',
    },
    header: {
        width: 360,
        height: 520,
        backgroundColor: '#51B9A0',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        position: 'absolute',
        top: 100,
        fontSize: 36,
        fontWeight: 'bold'
    },
    btnSignin: {
        position: 'absolute',
        top: 450,
        backgroundColor: 'black',
        height: 37,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        width: 123,
        borderRadius: 50

    },
    signin: {
        color: 'white',
        fontFamily: 'sans-serif',
    }

})