import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image, SafeAreaView, TextInput, onChangeText } from 'react-native';



export default function Login() {
    const [username, onChangeText] = React.useState(null);
    const [pass, onChangePass] = React.useState(null);
    return (
        <View style={styles.canvas} >
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.logo} source={require('../assets/zz1.png')}
                    />
                </View>
                <View style={styles.form}>
                    <Text style={styles.textLogin}>Email</Text>
                    <SafeAreaView>
                        <TextInput style={styles.input} onChangeText={onChangeText} value={username}>
                        </TextInput>
                    </SafeAreaView>
                    <Text style={styles.textLogin}>Password</Text>
                    <SafeAreaView>
                        <TextInput style={styles.input} onChangeText={onChangePass} value={pass}>
                        </TextInput>
                    </SafeAreaView>
                    <TouchableOpacity style={styles.btnSignin}
                        onPress={() => document.location.href = 'Login.js'}>
                        <Text style={styles.signin}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    canvas: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
    },
    container: {
        width: 360,
        height: 640,
        backgroundColor: 'white',
        alignItems: 'center',

    },
    header: {
        width: 360,
        height: 245,
        backgroundColor: '#51B9A0',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 140,
        height: 140,
        borderRadius: 100
    },
    btnSignin: {
        position: 'absolute',
        left: 75,
        top: 200,
        backgroundColor: 'black',
        height: 37,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        width: 123,
        borderRadius: 50,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'black'
    },
    signin: {
        fontFamily: 'sans-serif',
    },
    form: {
        marginTop: 100,
        width: 275,
        height: 100,
    },
    input: {
        width: 275,
        height: 30,
        borderWidth: 1,
        marginBottom: 30,
        paddingLeft: 15,
    }

})